#!/bin/bash
#SBATCH --job-name=EASEL
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-type=END
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load nextflow
module load singularity

SINGULARITY_TMPDIR=$PWD/tmp
export SINGULARITY_TMPDIR

nextflow run -hub gitlab PlantGenomicsLab/easel-benchmarking-nf  -profile singularity,xanadu -params-file params.yaml 