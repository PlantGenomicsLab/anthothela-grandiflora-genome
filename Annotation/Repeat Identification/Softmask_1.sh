#!/bin/bash
#SBATCH --job-name=repeat_db
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=70G
#SBATCH -o %x_%j.out                
#SBATCH -e %x_%j.err
#SBATCH --mail-type=ALL                  
#SBATCH --mail-user=                     

module load  singularity/biosim-3.10.0

singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif BuildDatabase -name "rm_database" /core/projects/EBP/conservation/gen_assembly_pipeline/flowerbud_test/test_outdir_flowerbud/04_purge/long_read/flye_FLOWERBUD_T1_filtered/purge_flye_FLOWERBUD_T1_filtered.fasta 