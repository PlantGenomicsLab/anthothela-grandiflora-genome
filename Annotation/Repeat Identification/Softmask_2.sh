#!/bin/bash
#SBATCH --job-name=repeat_modeler
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=50G
#SBATCH--mail-user=
#SBATCH -o %x_%j.out                
#SBATCH -e %x_%j.err

module load singularity/biosim-3.10.0 

# run with rm_database produced by Softmask1
singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif RepeatModeler -threads 30 -database rm_database