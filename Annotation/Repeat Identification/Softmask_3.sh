#!/bin/bash
#SBATCH --job-name=repeat_masker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=70G
#SBATCH --mail-type=END
#SBATCH--mail-user=
#SBATCH -o %x_%j.out                
#SBATCH -e %x_%j.err

module load singularity/3.9.2     

# use with output of Softmask 2
singularity exec /core/projects/EBP/software/TEtools/dfam-tetools-latest.sif RepeatMasker -dir repeatmasker_out -pa 16 -lib RM_*/consensi.fa.classified -gff -a -noisy -xsmall /core/projects/EBP/conservation/gen_assembly_pipeline/flowerbud_test/test_outdir_flowerbud/04_purge/long_read/flye_FLOWERBUD_T1_filtered/purge_flye_FLOWERBUD_T1_filtered.fasta