#!/bin/bash
#SBATCH --job-name=nextflow_flowerbud
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=START,END
#SBATCH --mem=10G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load nextflow

nextflow pull emilytrybulec/argonaut
nextflow run emilytrybulec/argonaut \
  -r main \
  -params-file my_params.yaml \
  -c my_config \
  -profile singularity,xanadu \
  -resume \
