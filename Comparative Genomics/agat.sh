#!/bin/bash
#SBATCH --job-name=agat
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mem=10G
#SBATCH --mail-type=END
#SBATCH--mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load singularity 

## for each GFF file do:
## convert transcript feature in 3rd column to mRNA ## 

sed 's/transcript/mRNA/g' /core/projects/EBP/conservation/flowerbud_coral/06_orthofinder/coral_species/x/x.gff > mRNA_x.gff

## use AGAT to pull longest isoform ##

singularity exec /isg/shared/databases/nfx_singularity_cache/depot.galaxyproject.org-singularity-agat-1.2.0--pl5321hdfd78af_0.img agat_sp_keep_longest_isoform.pl \
         -gff mRNA_x.gff -o longest_isoform_x.gff

## pull protein sequences from longest isoform GFF file ##
## replace -f with proper genome assembly *.fna ##

singularity exec /isg/shared/databases/nfx_singularity_cache/depot.galaxyproject.org-singularity-agat-1.2.0--pl5321hdfd78af_0.img agat_sp_extract_sequences.pl \
         -g longest_isoform_x.gff \
         -f /core/projects/EBP/conservation/flowerbud_coral/06_orthofinder/coral_species/x/x.fna \
         -p -o x.pep