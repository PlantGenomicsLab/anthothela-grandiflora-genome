#!/bin/bash
#SBATCH --job-name=cafe
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
echo -e "\nStart time:"
date

module load CAFE5/5.1.0

# Set the path to the input file and phylogenetic tree file
input_file=cafe.input.tsv
tree_file=SpeciesTree_rooted.txt.ultrametric.tre

# Run Cafe5 with the specified parameters

# error model:
cafe5 -i $input_file -t $tree_file -c 16 -p -e

# run 1:
cafe5 -i $input_file -t $tree_file -c 16 -p -eresults/Base_error_model.txt -o cafe_results_1e

# run 2:
cafe5 -i $input_file -t $tree_file -c 16 -p -eresults/Base_error_model.txt -o cafe_results_2e

# run 3:
cafe5 -i $input_file -t $tree_file -c 16 -p -eresults/Base_error_model.txt -o cafe_results_3e

echo -e "\nEnd time:"
date