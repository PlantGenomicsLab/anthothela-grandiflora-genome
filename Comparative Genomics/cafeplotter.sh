#!/bin/bash
#SBATCH --job-name=cafeplotter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

/home/FCAM/lhumphrey/.local/bin/cafeplotter -i cafe_results_1e -o cafeplotter_1e --format 'pdf'