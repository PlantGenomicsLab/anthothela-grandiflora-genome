#!/bin/bash
#SBATCH --job-name=eggnog
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH--mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load python/3.8.1

/home/FCAM/lhumphrey/eggnog-mapper/emapper.py -m diamond --dmnd_db /core/labs/Wegrzyn/IngaGenome/Eggnog-mapper/eggnog_proteins.dmnd --itype proteins \
         -i /core/projects/EBP/conservation/flowerbud_coral/06_orthofinder/cafe/longest_seqs.pep \
         -o longest_seqs --data_dir /core/labs/Wegrzyn/IngaGenome/Eggnog-mapper --cpu 12