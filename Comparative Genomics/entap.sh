#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH--mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load EnTAP/1.0.1

EnTAP --runP --ini $PWD/entap_config.ini -i /core/projects/EBP/conservation/flowerbud_coral/06_orthofinder/cafe/longest_seqs.pep \
        -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.208.dmnd --threads 16 --out-dir $PWD/longest_seqs_entap