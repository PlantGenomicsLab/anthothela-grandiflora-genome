#!/bin/bash
#SBATCH --job-name=of_filter
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH--mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load python/3.6.1
module load OrthoFinder/2.5.4

# prepare orthofinder output for filtering
awk -F'\t' '{print "(null)\t"$0}' /core/projects/EBP/conservation/flowerbud_coral/06_orthofinder/pep_files/OrthoFinder/Results_Apr01/Orthogroups/Orthogroups.GeneCount.tsv > tmp.tsv
awk -F'\t' '{$NF=""; print $0}' tmp.tsv | rev | sed 's/^\s*//g' | rev | tr ' ' '\t' > mod.tsv
awk 'NR==1 {$1="Desc"} 1' OFS='\t' mod.tsv > mod2.tsv

# run clade and size filter script
python clade_and_size_filter.py -i mod2.tsv -s -o cafe.input.tsv