#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 16
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=150G
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
echo -e "\nStart time:"
date

module load OrthoFinder/2.5.4

python $ORTHOFINDER -t 16 -f /core/projects/EBP/conservation/flowerbud_coral/06_orthofinder/pep_files

echo -e "\nEnd time:"
date