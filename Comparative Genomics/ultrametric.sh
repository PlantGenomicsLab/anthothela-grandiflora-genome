#!/bin/bash
#SBATCH --job-name=of_ultra
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH--mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load python/3.6.1
module load OrthoFinder/2.5.4

python make_ultrametric.py /core/projects/EBP/conservation/flowerbud_coral/06_orthofinder/pep_files/OrthoFinder/Results_Apr01/Species_Tree/SpeciesTree_rooted.txt \
        -r 565