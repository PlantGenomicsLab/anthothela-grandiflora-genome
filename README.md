# Great Flowerbud Coral Genome Assembly & Annotation

# Table of Contents
[[_TOC_]]

## Introduction

![Screenshot_2024-03-04_at_2.40.23_PM](/uploads/6f14866a3069e891866f0dfa105c6764/Screenshot_2024-03-04_at_2.40.23_PM.png)
> Image of a great flowerbud coral in the North Atlantic from NOAA.


*Anthothela grandiflora*, commonly known as the great flowerbud coral, is a species of cold-water coral found widespread deep in the northern Atlantic Ocean. Cold-water corals are important hotspots for ocean biodiversity and provide habitat for a variety of deep sea marine life. As with tropical coral reefs, these corals are vulnerable to anthropogenic threats including pollution and climate change. Cold-water corals may be even more susceptible to ocean acidification due to the lower saturation level of calcium carbonate in deep waters, leading to slower coral growth and weaker skeletons.

One challenge towards conservation of cold-water corals is the limited knowledge of coral taxonomy and genomics. Within the *Anthothela* genus there is much uncertainty over species delineation, which can be difficult to ascertain based on morphology alone. The group *Anthothela grandiflora* in particular is believed likely to contain other species due to its current large geographic range. Recent studies have assembled draft genomes for a variety of coral species, including the first cold-water coral genome *Trachythela* sp. Providing a genome assembly of *Anthothela grandiflora* could help to elucidate the phylogeny of cold-water corals and aid deep sea conservation measures. In addition, sequencing a deep sea octocoral could help investigate the genetic mechanisms behind calcification and how soft and stony corals respond differently to climate change.

With the development of third generation sequencing technologies, high quality reference genomes are now increasingly available to study species of conservation concern. Here we use Oxford Nanopore PromethION long read data and Illumina short read data to sequence the *Anthothela grandiflora* genome. We will begin by re-basecalling reads using the Dorado basecaller, followed by a hybrid assembly and long-read only assembly with the Nextflow pipeline. An assembly will then be chosen to annotate repeats and genes using the EASEL pipeline for further analysis.

## Final Genome Files
**Reads**

Raw DNA Reads FASTQ: 
`/core/projects/EBP/conservation/flowerbud_coral/assembly/flowerbud_rebasecall_reads.fastq.gz`

Filtered DNA Reads FASTQ:
`/core/projects/EBP/conservation/gen_assembly_pipeline/flowerbud_test/test_outdir_flowerbud/01_read_qc/centrifuge/filtered/FLOWERBUD_T1_filtered.fastq`

Trimmed Paired-End RNA Reads FASTQ: 
`/core/projects/EBP/conservation/flowerbud_coral/05_annotation/flowerbud_sickle_rna_1.fastq`
`/core/projects/EBP/conservation/flowerbud_coral/05_annotation/flowerbud_sickle_rna_2.fastq`

**Assembly**

Genome Assembly FASTA:
`/core/projects/EBP/conservation/gen_assembly_pipeline/flowerbud_test/test_outdir_flowerbud/04_purge/long_read/flye_FLOWERBUD_T1_filtered/purge_flye_FLOWERBUD_T1_filtered.fasta`

Softmasked Genome Assembly FASTA:
`/core/projects/EBP/conservation/flowerbud_coral/04_repeats/repeatmasker_out/purge_flye_FLOWERBUD_T1_filtered.fasta.masked`

**Annotation**

Genome Annotation Filtered Longest Isoform GFF:
`/core/projects/EBP/conservation/flowerbud_coral/05_annotation/EASEL/final_predictions/grandiflora_filtered_longest_isoform.gff`


## Assembly

### Oxford Nanopore Raw Reads
A specimen of Anthothela grandiflora was collected from Lydonia Canyon (0° 19.54’N, 67° 42.00’W, September 2018) in the Northeast Canyons and Seamounts Marine National Monument of the Atlantic Outer Continental Shelf by the exploration vessel Alucia at a depth of 646 meters. The sample was preserved by snap freezing with liquid nitrogen and stabilized using an RNA/DNA Shield collection kit. Genomic DNA was extracted from the sample of coral tissue and mucus by using the Phenol-Chloroform-Isoamyl Alcohol (P/C/IA) extraction method as well as mRNA extraction. SPRI size selection protocol was conducted to remove short DNA fragments through bead buffer exchange and select for fragments greater than 1.5-2 kb. High molecular weight (HMW) sequencing libraries were prepared for long reads using the ligation sequencing gDNA kit (SQK-LSK109, Oxford Nanopore Technologies). Whole genome sequencing (WGS) and mRNA sequencing libraries were prepared for short reads using the Nextera XT library preparation kit (Illumina). Separate loads of the ONT library were loaded onto R9.4 flow cells for one MinION and two PromethION runs. 

#### Dorado Re-Basecalling

Oxford Nanopore sequencing involves the movement of a DNA strand through a pore to record changes in the electrical current, which correspond to individual nucleotide bases. The changing current signals are used to determine the nucleotide sequence of the raw read data, which can be translated into a sequence of bases using basecalling algorithms. Dorado is an open-source basecaller tool used to improve basecalling algorithms for Oxford Nanopore reads. We will use Dorado to re-basecall our long reads and improve the accuracy of our coral genome data. In order to use this basecalling tool, the MinION and PromethION raw reads were converted from the fast5 file format to the pod5 file format compatible with Dorado.

Dorado Scripts: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/tree/main/Raw%20Reads

#### Quality Control

The results of running Nanoplot quality control on our raw reads before and after Dorado re-basecalling show that the number of reads and total bases did increase slightly for all MinION and PromethION sequencing runs, although the mean read quality and N50 values tended to decrease. 

| Raw Reads Nanoplot Summary | Minion 1      | Promethion 1a  | Promethion 1b | Promethion 1c | Promethion 1d |
|----------------------------|---------------|----------------|---------------|---------------|---------------|
| Mean read length           | 6,076.4       | 3,360.1        | 3,152.2       | 2,944.2       | 3,207.7       |
| Mean read quality          | 14.9          | 13.9           | 12.9          | 14.1          | 12.5          |
| Median read length         | 4,463         | 2,246          | 2,105         | 1,617         | 1,888         |
| Median read quality        | 15.1          | 13.8           | 12.6          | 14.2          | 12.2          |
| Number of reads            | 483,427       | 6,963,751      | 1,298,174     | 1,531,216     | 2,418,073     |
| Read length N50            | 8,294         | 4,781          | 4,467         | 5,844         | 6,119         |
| STDEV read length          | 5,396.6       | 3,324.4        | 3,170.4       | 3,453.0       | 3,550.2       |
| Total bases                | 2,937,502,930 | 23,398,784,996 | 4,092,075,908 | 4,508,230,183 | 7,756,477,279 |

| Re-basecalls Nanoplot Summary | Minion 1         | Promethion 1a     | Promethion 1b    | Promethion 1c    | Promethion 1d    |
|-------------------------------|------------------|-------------------|------------------|------------------|------------------|
| Mean read length              | 6,065.90         | 3,361.40          | 3,204.30         | 2,961.90         | 3,229.60         |
| Mean read quality             | 14.8             | 13.8              | 12.7             | 13.9             | 12.2             |
| Median read length            | 4,453.00         | 2,246.00          | 2,146.00         | 1,633.00         | 1,915.00         |
| Median read quality           | 15               | 13.8              | 12.5             | 14               | 12               |
| Number of reads               | 488,119.00       | 7,050,470.00      | 1,311,648.00     | 1,553,823.00     | 2,506,220.00     |
| Read length N50               | 8,287.00         | 4,781.00          | 4,531.00         | 5,846.00         | 6,132.00         |
| STDEV read length             | 5,395.50         | 3,326.10          | 3,202.80         | 3,465.30         | 3,563.60         |
| Total bases                   | 2,960,859,766.00 | 23,699,494,246.00 | 4,202,935,434.00 | 4,602,254,143.00 | 8,093,972,256.00 |

### Argonaut Nextflow Assembly Pipeline

The [Argonaut](https://github.com/emilytrybulec/argonaut) nextflow assembly pipeline will be used to perform genome assembly of our long and short read data using various bioinformatics tools. The pipeline also includes quality control checking at each step to assess the completeness, contiguity, and correctness of our reference genome. We will first perform a long read only assembly, and then compare with a hybrid long and short read assembly to identify the best quality genome. In order to input our re-basecalled long read data into the Argonaut pipeline, we converted the Dorado output from the bam file format to fastq file format and concatenated all read files into a single fastq file.

Argonaut Scripts: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/tree/main/Assembly

#### Long Read Quality Control
The first step in the Argonaut assembly pipeline was to estimate the genome size and coverage depth of reads using Kmer-freq and GCE at k-mer size 21. Centrifuge was then used within the pipeline to filter and remove potential contaminants from the reads, including bacterial and fungal associations. Recentrifuge and Pavian were used to visualize the taxonomic ranks of organisms removed from the read data and identify the top contaminants. NanoPlot quality assessment was again performed to measure read quality before and after contaminant removal.

##### Genome Size Estimation:
| Effective kmer species | Effective kmer individuals | Coverage depth | Genome size |
|------------------------|----------------------------|----------------|-------------|
| 334790856              | 35117387016                | 35.9356        | 9.77232e+08 |

##### NanoPlot Summary Results:
|                       | Unfiltered Reads | Filtered Reads   |
|-----------------------|------------------|------------------|
| Mean read length:     | 3,374.0          | 3,366.9          |
| Mean read quality:    | 13.4             | 13.4             |
| Median read length:   | 2,202.0          | 2,198.0          |
| Median read quality:  | 13.3             | 13.3             |
| Number of reads:      | 12,910,280.0     | 12,860,111.0     |
| Read length N50:      | 5,342.0          | 5,329.0          |
| STDEV read length:    | 3,520.9          | 3,513.4          |
| Total bases:          | 43,559,515,845.0 | 43,298,438,636.0 |

##### Unfiltered Reads NanoPlot:
![FLOWERBUD_T1LengthvsQualityScatterPlot_dot](/uploads/27cf03501ed22c35bf72bc5cd707c35a/FLOWERBUD_T1LengthvsQualityScatterPlot_dot.png)

##### Filtered Reads NanoPlot:
![FLOWERBUD_T1_filteredLengthvsQualityScatterPlot_dot](/uploads/742a5df94b7533acd0d034f31d84e1a6/FLOWERBUD_T1_filteredLengthvsQualityScatterPlot_dot.png)

PromethION sequencing generated a total of 43.6 Gb of Oxford Nanopore long reads at coverage depth 35.94x with an estimated genome size of 977 Mb. Filtering for contaminants removed bacterial and fungal associations including Botrytis cinerea, Porphyrobacter sp., and Acinetobacter baumannii, resulting in 43.3 Gb of long reads with an N50 length of 5.33 Kb.

##### Centrifuge Contaminant Removal Sankey Plot:

<img src="/uploads/b9f6d29be61181696d4c22003bf9ca5d/Screenshot_2024-02-09_at_2.20.23_PM.png" alt="Alt text" width="800" height="400" />

#### Flye Assembly Quality Control
Next the pipeline generated a de novo genome assembly using the Flye long read assembly tool. Racon was applied within the pipeline to correct errors and polish the Flye-generated assembly. Following error correction, PurgeHaplotigs was used to reduce allele-based duplication in the Flye assembly. Quality checking was performed at each step of the pipeline using QUAST, BUSCO against the Metazoa single-copy ortholog database, and Merqury using the Nanopore reads as a database to assess the completeness, contiguity, and correctness of the genome assembly leading into downstream annotation and analysis. 

##### QUAST Output:
|  QUAST Statistics | Flye Assembly | Flye + Racon Polish | Flye + Purge |
|:-----------------:|:-------------:|:-------------------:|--------------|
| Number of contigs | 37369         | 36539               | 22669        |
| Largest contig    | 1247916       | 1236939             | 1247916      |
| Total length      | 737902610     | 716423068           | 641370213    |
| GC (%)            | 37.42         | 37.47               | 37.44        |
| N50               | 70301         | 68693               | 82570        |
| L50               | 2697          | 2669                | 2076         |
| # N's per 100 kbp | 0             | 0                   | 0            |

<img src="/uploads/a88eed60e896683a278309ecb395e9a5/Screenshot_2024-02-12_at_1.39.05_PM.png" alt="Alt text" width="600" height="500" />

##### BUSCO Output:
| BUSCO Statistics                    | Flye Assembly | Flye + Racon Polish | Flye + Purge |
|-------------------------------------|:-------------:|:-------------------:|--------------|
| Complete BUSCOs (C)                 | 806 (84.5%)   | 794 (83.2%)         | 802 (84.1%)  |
| Complete and single-copy BUSCOs (S) | 708 (74.2%)   | 709 (74.3%)         | 767 (80.4%)  |
| Complete and duplicated BUSCOs (D)  | 98 (10.3%)    | 85 (8.9%)           | 35 (3.7%)    |
| Fragmented BUSCOs (F)               | 82 (8.6%)     | 87 (9.1%)           | 81 (8.5%)    |
| Missing BUSCOs (M)                  | 66 (6.9%)     | 73 (7.7%)           | 71 (7.4%)    |
| Total BUSCO groups searched         | 954           | 954                 | 954          |

##### Merqury Output:
| Merqury Statistics | Flye Assembly | Flye + Racon Polish | Flye + Purge |
|:------------------:|:-------------:|:-------------------:|--------------|
| Quality score      | 37.0977       | 34.2058             | 37.1181      |

<img src="/uploads/24fd30a7ed33c48794608d727e4c63f9/flye_FLOWERBUD_T1_filtered.flye_FLOWERBUD_T1_filtered.spectra-cn.fl.png" alt="Alt text" width="600" height="500" />

Initial Flye assembly of the reads through Argonaut produced a total BUSCO completeness of 84.5% with an N50 length of 70.3 Kb and quality score of 37.1. Polishing of Flye with Medaka decreased BUSCO completeness by 1.3%, decreased N50 length by 1.61 Kb, and decreased quality score by 2.89, therefore error correction was excluded from the final assembly.

##### Purge Haplotigs Histogram:

<img src="/uploads/5a19add4f5a40603273c367a44d91178/flye_FLOWERBUD_T1_filtered.aligned.bam.histogram.png" alt="Alt text" width="700" height="500" />

Purge Haplotigs effectively resolved heterozygosity by maintaining overall BUSCO completeness and improving contiguity to give an N50 length of 8.26 Kb in 22,669 scaffolds. The final scaffold-level Flye assembly produced by Argonaut is reported at 641 Mb in length with BUSCO completeness of 84.1% (80.4% single-copy, 3.7% duplicated, 8.5% fragmented, and 7.4% missing), N50 length of 8.26 Kb, and quality score of 37.1.

##### Blobtools Output:

<img src="/uploads/6bf7004845489bccea377262caa9dcab/db_purge_flye_FLOWERBUD_T1_filtered.fasta_snail.png" alt="Alt text" width="500" height="500" />

<img src="/uploads/ee1c89bb2588741ae3d84a1e08aed9ba/db_purge_flye_FLOWERBUD_T1_filtered.fasta_cumulative.png" alt="Alt text" width="500" height="500" />

## Annotation

### Repeat Identification

RepeatModeler was used to perform repeat identification on the final Flye assembly produced by Argonaut. The resulting set of consensus sequences was used as input with RepeatMasker in order to softmask repetitive regions of the genome and prepare for annotation.

Repeat Scripts: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/tree/main/Annotation/Repeat%20Identification

| RepeatMasker Statistics    | Number of elements | Length occupied (bp) | Percentage of sequence (%) |
|----------------------------|--------------------|----------------------|----------------------------|
| Retroelements              | 213515             | 86399048             | 13.47                      |
| LINEs                      | 118261             | 52471709             | 8.18                       |
| LTR elements               | 85764              | 32559876             | 5.08                       |
| DNA transposons            | 68281              | 21821946             | 3.4                        |
| Unclassified               | 1403190            | 319066663            | 49.75                      |
| Total interspersed repeats | n/a                | 427287657            | 66.62                      |

Following identification of repeat sequences by RepeatModeler, RepeatMasker softmasked 434.9 Mb (67.80%) of the great flowerbud genome. The softmasked regions were composed of 13.47% retroelements, most of which represented long interspersed nuclear elements (LINEs) at 8.18%, followed by long terminal repeats (LTRs) at 5.08%. A large portion of the repeats were unclassified (49.75%).

### EASEL Pipeline
The softmasked Flye genome assembly and trimmed paired end mRNA reads were used as input for the nextflow genome annotation pipeline [EASEL](https://gitlab.com/PlantGenomicsLab/easel) to generate reference annotations. Within the pipeline, the mRNA reads were aligned to the reference genome using HISAT2, then StringTie2 and PsiCLASS were used to assemble transcripts. Transcriptome hints were used to train AUGUSTUS with the ‘invertebrate’ dataset and generate unfiltered and filtered structural annotation files. EnTAP was also used to functionally annotate the predicted transcripts with proteins from the RefSeq database. Output from EASEL gave statistics on the total number of genes and transcripts, EnTAP alignment rate, mono-exonic and multi-exonic rates, and BUSCO completeness for the primary isoform and longest isoform which were used to select the superior reference annotation.

EASEL Scripts: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/tree/main/Annotation/EASEL

| Annotation Statistics            | Filtered (Longest Isoform) | Filtered (Primary Isoform) | Unfiltered | Filtered |
|----------------------------------|----------------------------|----------------------------|------------|----------|
| Total number of genes            | 14837                      | 14837                      | 38076      | 14837    |
| Total number of transcripts      | n/a                        | n/a                        | 95569      | 43238    |
| EnTAP alignment rate             | 0.79                       | 0.8                        | 0.66       | 0.82     |
| Mono-exonic/multi-exonic rate    | 0.1                        | 0.11                       | 0.66       | 0.09     |
| BUSCO % complete (metazoa_odb10) | 81.80%                     | 79.20%                     | 90.00%     | 84.00%   |

Mapping the mRNA reads to the softmasked genome resulted in 96.51% alignment rate. The trimmed paired end mRNA reads used for annotation contained 32,943,400 total bases with a mean read length of 98 and GC content of 0.44%. The filtered longest isoform was of slightly higher quality with 14,837 total genes, 0.79 EnTAP alignment rate, 0.10 mono-exonic rate, and 81.8% total BUSCO completeness, leading this annotation to be selected for comparative analysis.

## Comparative Genomics

### Gene Family Analysis
To identify orthologous gene families among different corals, reference genomes and annotations were acquired for eleven other species, including two cold-water corals (Desmophyllum pertusum, Eunicella verrucosa), three octocorals (Dendronephthya gigantea, Xenia sp., Paramuricea clavata), five hexacorals (Acropora cervicornis, Pocillopora meandrina, Stylophora pistillata, Orbicella faveolata, Acropora digitifera), and one hydra (Hydra vulgaris). Quality assessment was performed to ensure each genomic reference passed the threshold of 70% BUSCO completeness for inclusion in the gene family analysis.

#### AGAT GFF File Conversions
In preparation for phylogeny analysis with OrthoFinder, the AGAT toolkit was used to pull the longest isoforms for each coral species from their GFF genome annotation files and convert these to protein sequence files.

AGAT Script: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/blob/main/Comparative%20Genomics/agat.sh

#### OrthoFinder Species Tree Inference
The resulting protein files along with the de novo annotated *A. grandiflora* proteins were used as input for OrthoFinder to identify orthologous gene families for comparative analysis. Using the resulting gene trees, a species phylogeny was constructed by OrthoFinder and rooted using the species tree inference from all genes (STAG) methodology. The species tree was made ultrametric using a root divergence time of 565 MYA based on the estimate from TimeTree. 


OrthoFinder Script: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/blob/main/Comparative%20Genomics/orthofinder.sh

Ultrametric Script: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/blob/main/Comparative%20Genomics/ultrametric.sh

##### Species Tree Phylogeny and Upset Plot:

<img src="/uploads/0c93cbe8c542176e87e2fcc171e74688/Screenshot_2024-06-11_at_12.53.00_PM.png" alt="Alt text" width="704" height="308" />

OrthoFinder analysis conducted with eleven coral and one hydra species assigned 303,100 genes (85.7% of the total) into 24,955 orthogroups. Half of all the genes were assigned to orthogroups containing 14 or more genes and were present in the largest 5,234 orthogroups. A total of 4,664 orthogroups had all species present, with 917 of these groups consisting entirely of single-copy genes. The 4,664 gene trees with all species present were used by STAG to infer the species tree. The resulting consensus tree phylogeny verifies previous trends in the divergence between classes of soft *Octocorallia* and stony *Hexacorallia*, as well as the *Hydroidolina* outgroup. 

#### Filtering by Clade and Size
To prepare input for CAFE v5, the orthogroups were filtered by clade and size to remove gene families representing only one species and gene families with over 100 genes representing a single species. Filtering of orthogroups by clade and size resulted in 23,590 gene families to be used as input for CAFE v5.

Filtering Script: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/blob/main/Comparative%20Genomics/filter.sh

#### CAFE5 Gene Family Expansions and Contractions
The ultrametric species tree and 23,590 filtered orthogroups were used as input for CAFE v5 to identify evidence of significantly expanding or contracting gene families using the base model at P-values <0.01. Gene family analysis using CAFE v5 detected 5 expanded and 155 contracted gene families specific to *A. grandiflora*.

CAFE5 Script: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/blob/main/Comparative%20Genomics/cafe.sh

CAFE Plotter Script: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/blob/main/Comparative%20Genomics/cafeplotter.sh


#### EnTAP & EggNOG Gene Family Annotation 
The significantly changing gene families for *A. grandiflora* were annotated using EnTAP and EggNOG-mapper to assign putative functions to the longest gene from each orthogroup.

EnTAP Script: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/blob/main/Comparative%20Genomics/entap.sh

EggNOG Script: https://gitlab.com/PlantGenomicsLab/anthothela-grandiflora-genome/-/blob/main/Comparative%20Genomics/eggnog.sh

|                        COG Description                        | Number of Expanding Gene Families | Number of Contracting Gene Families |
|:-------------------------------------------------------------:|:---------------------------------:|:-----------------------------------:|
|              Amino acid transport and metabolism              |                 0                 |                  3                  |
|             Carbohydrate transport and metabolism             |                 2                 |                  2                  |
|   Cell cycle control, cell division, chromosome partitioning  |                 0                 |                  4                  |
|             Cell wall/membrane/envelope biogenesis            |                 0                 |                  2                  |
|                Chromatin structure and dynamics               |                 0                 |                  4                  |
|                          Cytoskeleton                         |                 0                 |                  5                  |
|                       Defense mechanisms                      |                 0                 |                  2                  |
|                Energy production and conversion               |                 0                 |                  1                  |
| Intracellular trafficking, secretion, and vesicular transport |                 1                 |                  1                  |
|                 Lipid transport and metabolism                |                 0                 |                  4                  |
|  Posttranslational modification, protein turnover, chaperones |                 1                 |                  8                  |
|             Replication, recombination and repair             |                 0                 |                  15                 |
|                RNA processing and modification                |                 0                 |                  2                  |
|  Secondary metabolites biosynthesis, transport and catabolism |                 0                 |                  4                  |
|                 Signal transduction mechanisms                |                 0                 |                  15                 |
|                         Transcription                         |                 0                 |                  7                  |
|        Translation, ribosomal structure and biogenesis        |                 0                 |                  2                  |
|                        Unknown function                       |                 1                 |                  79                 |

The small set of expanding orthogroups were broadly described by COG categories including biological regulation, response to stimuli and DNA damage, metabolic and biosynthetic processes, protein transport and localization, and ion binding. Genetic functions among the contracted gene families related to metabolic and biosynthetic processes, immune and stress response, symbiosis, and compound binding. 

Examination of the contracted gene families related to compound binding revealed two gene families associated with calcium ion binding, which could provide meaningful insight into the evolution of coral calcification mechanisms in A. grandiflora. Further investigation into the functions of these rapidly changing gene families could reveal more about cold-water octocoral biomineralization and differential responses to ocean acidification at the genomic level.

