#!/bin/bash
#SBATCH --job-name=gzip
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname

minion1="/archive/projects/EBP/roneill/reads/nanopore/minion/coral_minion/03Oct2019_Coral_MIN106_FAK10514_LSK109/dorado0-3-4_rebasecall_sup/minion1_basecalls.fastq"

promethion1="/archive/projects/EBP/roneill/coral/primnoidae_N676Q13C001/2020Feb25_N676Q13COO1Coral_LSK109_PAE31530/2020Feb25_N676Q13COO1Coral_LSK109_PAE31530/20200225_2137_2-E7-H7_PAE31530_de9a7586/dorado0-3-4_rebasecall_sup/promethion1_basecalls.fastq"

promethion2="/archive/projects/EBP/roneill/coral/primnoidae_N676Q13C001/2020Feb27_Coral_LSK109_PAE31530_2/2020Feb27_Coral_LSK109_PAE31530_2/20200227_1943_2-E7-H7_PAE31530_40590462/dorado0-3-4_rebasecall_sup/promethion2_basecalls.fastq"

promethion3="/archive/projects/EBP/roneill/coral/primnoidae_N676Q13C001/2020JUN01_N676-Q13-C001_ss/2020JUN01_N676-Q13-C001_ss_PAE78569_LSK109/20200601_1806_1A_PAE78569_b4110092/dorado0-3-4_rebasecall_sup/promethion3_basecalls.fastq"

promethion4="/archive/projects/EBP/roneill/coral/primnoidae_N676Q13C001/2020JUN01_N676-Q13-C001_ss/2020JUN01_N676_Q13_Cool_ss_PAE78569_LSK109_2/2020JUN01_N676_Q13_Cool_ss_PAE78569_LSK109_2/20200602_1655_1A_PAE78569_3a571c9a/dorado0-3-4_rebasecall_sup/promethion4_basecalls.fastq"

output_file="/core/projects/EBP/conservation/flowerbud_coral/assembly/flowerbud_rebasecall_reads.fastq" 

cat "$minion1" "$promethion1" "$promethion2" "$promethion3" "$promethion4" > "$output_file"

gzip flowerbud_rebasecall_reads.fastq
