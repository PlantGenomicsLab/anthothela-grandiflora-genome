#!/bin/bash
#SBATCH --job-name=convert_p1_fastq
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=40G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load samtools/1.16.1

samtools bam2fq /archive/projects/EBP/roneill/coral/primnoidae_N676Q13C001/2020Feb25_N676Q13COO1Coral_LSK109_PAE31530/2020Feb25_N676Q13COO1Coral_LSK109_PAE31530/20200225_2137_2-E7-H7_PAE31530_de9a7586/dorado0-3-4_rebasecall_sup/promethion1_basecalls.bam > promethion1_basecalls.fastq
