#!/bin/bash
#SBATCH --job-name=convert_p1_pod5
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=75G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


pod5 convert fast5 ./fast5_pass/*.fast5  --output output_pod5s/ --one-to-one ./fast5_pass/
