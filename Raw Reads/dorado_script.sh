#!/bin/bash
#SBATCH --job-name=dorado_m1_flowerbud
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=80G
#SBATCH --partition=gpu
#SBATCH --qos=general
#SBATCH --constraint="gpu_A10"
#SBATCH --mail-type=END
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err


module load Dorado/0.3.4

CUDA_LAUNCH_BLOCKING=1
export 'PYTORCH_CUDA_ALLOC_CONF=max_split_size_mb:512'

dorado download --model dna_r9.4.1_e8_sup@v3.3

dorado basecaller --batchsize 32 dna_r9.4.1_e8_sup@v3.3 /core/projects/EBP/conservation/flowerbud_coral/rebasecalled_reads/output_pod5s --modified-bases 5mCG_5hmCG --min-qscore 10 > minion1_basecalls.bam
