#!/bin/bash
#SBATCH --job-name=nanoplot_p4_dorado
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=35G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load NanoPlot/1.33.0

NanoPlot --ubam /archive/projects/EBP/roneill/coral/primnoidae_N676Q13C001/2020JUN01_N676-Q13-C001_ss/2020JUN01_N676_Q13_Cool_ss_PAE78569_LSK109_2/2020JUN01_N676_Q13_Cool_ss_PAE78569_LSK109_2/20200602_1655_1A_PAE78569_3a571c9a/dorado0-3-4_rebasecall_sup/promethion4_basecalls.bam --loglength -o nanoplots-p4 -p nano-p4 -t 4
