#!/bin/bash
#SBATCH --job-name=read_trim
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 4
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load fastp/0.23.2

fastp \
       -i /seqdata/EBP/animal/invertebrate/anthothela_grandiflora/N675-Q13-C001/illumina/wgs/WGS_2021Apr08/Coral-WGS_S14_L002_R1_001.fastq.gz \
       -I /seqdata/EBP/animal/invertebrate/anthothela_grandiflora/N675-Q13-C001/illumina/wgs/WGS_2021Apr08/Coral-WGS_S14_L002_R2_001.fastq.gz \
       -o ./trim_output/AntGran_wgs_trim_R1.fastq.gz \
       -O ./trim_output/AntGran_wgs_trim_R2.fastq.gz \
       --thread 4 -g -c -y 30 \
       --html ./trim_output/AntGran_wgs_trim_quality_report.html \
       --report_title AntGran_wgs_trim_report

module load fastqc/0.12.1

fastqc --dir ./tmp -t 4 \
       --outdir ./fastqc \
       ./trim_output/AntGran_wgs_trim_R*.fastq.gz

module load MultiQC/1.15 

multiqc ./fastqc \
        -o ./MultiQC \
        -n multiqc_report_AntGran.html